<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePdsRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pds_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('IDAPP')->unique()->nullable();
            $table->string('SURNAME')->nullable();
            $table->string('FIRSTNAME')->nullable();
            $table->string('MI')->nullable();
            $table->string('NAMEEXT')->nullable();
            $table->date('DATEOFBIRTH')->nullable();
            $table->string('PLACEOFBIRTH')->nullable();
            $table->string('SEX')->nullable();
            $table->string('CITIZENSHIP')->nullable();
            $table->string('CIVILSTATUS')->nullable();
            $table->string('RESADDRESS')->nullable();
            $table->string('PERADDRESS')->nullable();
            $table->string('HEIGHT')->nullable();
            $table->string('WEIGHT')->nullable();
            $table->string('TELNO')->nullable();
            $table->string('CPNO')->nullable();
            $table->string('EMAIL')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pds_records');
    }
}
