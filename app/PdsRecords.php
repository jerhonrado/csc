<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PdsRecords extends Model
{
    protected $fillable = ['IDAP', 'SURNAME', 'FIRSTNAME', 'MI', 'NAMEEXT', 'DATEOFBIRTH', 'PLACEOFBIRTH', 'SEX', 'CITIZENSHIP', 'CIVILSTATUS', 'RESADDRESS', 'PERADDRESS', 'HEIGHT', 'WEIGHT', 'TELNO', 'CPNO', 'EMAIL'];
}
