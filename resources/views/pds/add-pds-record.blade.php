@extends('layouts.master')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <form id="defaultForm" action="#" method="post" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="box-body">
                        <fieldset>
                            <h4>Add New PDS</h4>
                            <hr>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Record ID</label>
                                        <input type="text" name="appointee_id" class="form-control" required="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" name="appointee_surname" class="form-control" placeholder="Enter Last Name" required="">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" name="appointee_givenname" class="form-control" placeholder="Enter First name" required="">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Middle Initial</label>
                                        <input type="text" name="appointee_middlename" class="form-control" placeholder="Enter Middle initial" required="">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Ext Name</label>
                                        <input type="text" name="appointee_extname" class="form-control" placeholder="Ext Name">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Agency</label>
                                        <select name="appointee_agency" class="form-control select2" required="">
                                            <option value="">-- Select Agency --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Position</label>
                                        <select name="appointee_position" class="form-control select2" required="">
                                            <option value="">-- Select Position --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nature</label>
                                        <select name="appointee_nature" class="form-control select2" required="">
                                            <option value="">-- Select Nature --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Status</label>

                                        <select name="appointee_status" class="form-control select2" required="">
                                            <option value="">-- Select Status --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <script type="text/javascript">
                                $(document).ready(function(){  
                                    $.datepicker.setDefaults({  
                                        dateFormat: 'yy/mm/dd'   
                                    });  
                                    $(function(){  
                                        $("#appointment").datepicker();  
                                        $("#promotion").datepicker();  
                                    });  
                                });
                            </script>

                            <div class="row">
                                <div class="col-sm-6">
                                    
                                    <!-- Date dd/mm/yyyy -->
                                    <div class="form-group">
                                        <label>Date Received</label>

                                        <div class="input-group">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="text" name="appointee_datereceived" class="form-control" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.form group -->
                                </div>
                                <div class="col-sm-6">
                                    <label>Date Signed</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="datesigned" class="form-control" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                           </div>


                             <script>
                                $(function(){
                                      
                                    $('input[id$="endTime"]').inputmask({
                                        mask: "h:s t\\m",
                                        placeholder: "hh:mm xm - hh:mm xm",
                                        alias: "datetime",
                                        hourFormat: "12"
                                    });
                                  
                                  
                                });

                                $(function(){
                                      
                                    $('input[id$="endTime2"]').inputmask({
                                        mask: "h:s t\\m",
                                        placeholder: "hh:mm xm - hh:mm xm",
                                        alias: "datetime",
                                        hourFormat: "12"
                                    });
                                  
                                  
                                });
                            </script>
                            

                           <div class="row">    
                                <div class="col-sm-6">
                                    <!-- time Picker -->
                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <label>Time received:</label>
                                            <div class="input-group">
                                                <input type="text"  id="endTime" name="appointee_timereceived" class="form-control" required>

                                                <!-- <input type="text" name="appointee_timereceived" class="form-control timepicker" required=""> -->
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                        <!-- /.input group -->
                                        </div>
                                    <!-- /.form group -->
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <!-- time Picker -->
                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <label>Time signed:</label>
                                            <div class="input-group">
                                                <!-- <input type="text"  id="appointee_timesigned" name="appointee_timesigned" class="form-control time12-mask" required> -->

                                                <input type="text"  id="endTime2" name="appointee_timesigned" class="form-control" required>

                                                <!-- <input type="text" name="appointee_timesigned" class="form-control timepicker" required=""> -->
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                        <!-- /.input group -->
                                        </div>
                                    <!-- /.form group -->
                                    </div>
                                </div>
                            </div>

                           <div class="row">
                                <div class="col-sm-12">
                                    <!-- time Picker -->
                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <button type="submit" name="submit" class="btn ink-reaction btn-floating-action btn-lg btn-primary pull-right">
                                                <i class="fa glyphicon-plus"></i>
                                            </button>
                                        <!-- /.input group -->
                                        </div>
                                    <!-- /.form group -->
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </form>
        <!-- /.nav-tabs-custom -->
        </div>
    <!-- /.col -->
    </div>
</section>
<!-- /.content -->

@endsection