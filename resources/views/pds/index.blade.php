@extends('layouts.master')

@section('content')

    <section class="content-header">
      <h1>
        Personal Data Sheet
        <small>Master File</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Personal Data Sheet</li>
      </ol>
    </section>

<section class="content">
    <div class="row">    
	<div class="box">
        <div class="box-header">
          <h3 class="box-title">List of all records</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>ID</th>
              <th>LAST NAME</th>
              <th>FIRST NAME</th>
              <th>MI</th>
              <th>EXT NAME</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($pdsrecords as $pds)
              <tr>
                <td>{{$pds->id}}</td>
                <td>{{$pds->SURNAME}}</td>
                <td>{{$pds->FIRSTNAME}}</td>
                <td>{{$pds->MI}}</td>
                <td>{{$pds->NAMEEXT}}</td>
                <td>
                  <button class="btn btn-info" data-mytitle="{{$pds->title}}" data-mydescription="{{$pds->description}}" data-catid={{$pds->id}} data-toggle="modal" data-target="#edit">Edit</button>
                  /
                  <button class="btn btn-danger" data-catid={{$pds->id}} data-toggle="modal" data-target="#delete">Delete</button>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
    </div>
      <!-- /.box -->
    </div>
</section>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="float:right;">
  Add New Records
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New Record</h4>
      </div>
      <form action="{{route('pds-records.store')}}" method="post">
          {{csrf_field()}}
        <div class="modal-body">
        @include('pds.form')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection